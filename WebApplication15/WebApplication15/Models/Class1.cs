﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication15.Models
{
    public class Class1
    {
        public bool success;
        public string message;
     

        public List<string> tags;
        public List<int> values;
        public Class1(int n) 
            { success = false;
           
              message = "Program Initialised";
             initOthers(n);
            }
        private void initOthers(int n) {
            try
            {
                tags = new List<string>();
                values = new List<int>();
                Random r = new Random();
                for (int a = 1; a <= 24; a++)
                {
                    var stuff = r.Next() % 100;
                    values.Add(stuff);
                    tags.Add("Hour" + a.ToString());

                }
                success = true;
                message = "Executed successfully";
            }
            catch (Exception ex) { message = ex.Message; }
        }
    }
}